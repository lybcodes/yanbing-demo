@SpringBootApplication
@EnableRedisRepositories
public class FlowApplication {
    public static void main(String[] args) {
        SpringApplication.run(FlowApplication.class, args);
    }

}
